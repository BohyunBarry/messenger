package com.mobile.c.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by BohyunBartowski on 3/9/2018 0009.
 */

public class Room {
    public ArrayList<String> member;
    public Map<String, String> groupInfo;

    public Room(){
        member = new ArrayList<>();
        groupInfo = new HashMap<String, String>();
    }
}
