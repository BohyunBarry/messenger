package com.mobile.c.model;

/**
 * Created by BohyunBartowski on 3/9/2018 0009.
 */

public class User {
    public String name;
    public String email;
    public String avata;
    public Status status;
    public Message message;

    public User(){
        status = new Status();
        message = new Message();
        status.isOnline = false;
        status.timestamp = 0;
        message.idReceiver = "0";
        message.idSender = "0";
        message.text = "";
        message.timestamp = 0;
    }
}
